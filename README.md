# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* System dependencies
- Ruby Version 2.7.2

* How to Run
- Make sure you have Ruby Version 2.7.2 installed
- Make sure bundler installed run, `gem install bundler` to install it.
- Clone The Application
- Navigate to Application directory
- Copy `.env.example` file to `.env` file
- Run `bundle install` to install gems
- Run `rails s` to start the application
- Navigate on your browser to `localhost:3000`