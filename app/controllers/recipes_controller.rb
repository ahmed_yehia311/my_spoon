class RecipesController < ApplicationController
  def index
    @recipes = Recipe.all
  end

  def show
    @recipe = Recipe.find(params[:id])
    if @recipe.nil?
      flash[:error] = "Recipe not found"
      redirect_to :recipes
    end
  end
end