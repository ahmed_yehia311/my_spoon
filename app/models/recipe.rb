# Value Object
class Recipe
  attr_reader :id, :title, :image, :tags, :description, :chef_name

  def initialize(id:, title:, image:, tags: nil, description: nil, chef_name: nil)
    @id, @title, @image, @tags, @description, @chef_name = id, title, image, tags, description, chef_name
  end

  def self.all
    ContentDelivery::ListRecipes.new.call
  end

  def self.find(id)
    ContentDelivery::ShowRecipe.new.call(id)
  end
end