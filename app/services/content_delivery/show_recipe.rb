class ContentDelivery::ShowRecipe
  include ContentDelivery::Base
  
  def request_params(id)
    default_params.merge({
      content_type: recipe_content_id,
      select: "fields.title,fields.photo,sys.id,fields.tags,fields.description,fields.chef",
      "sys.id" => id
    })
  end

  def call(id)
    status, response = get_request(url: endpoints[:entries],
      headers: default_headers,
      params: request_params(id)
    )

    status ? parse_response(response.body) : nil
  end

  # Extract photo data from response
  def get_photo(response_body)
    puts response_body["items"][0]['fields']['photo']['sys']['id']
    id = response_body["items"][0].dig('fields','photo','sys','id')
    return nil unless id

    photo = response_body['includes']['Asset'].select {|asset| asset['sys']['id'] == id }[0]
    {
      title: photo['fields']['title'],
      url: photo['fields']['file']['url']
    }
  end

  # Extract tags data from response
  def get_tags(response_body)
    return nil unless response_body["items"][0].dig('fields', 'tags')

    ids = response_body["items"][0]['fields']['tags'].map { |tag| tag['sys']['id']}
    
    tags = response_body['includes']['Entry'].select {|asset| ids.include?(asset['sys']['id']) }
    tags.map { |tag| tag['fields']['name']}
  end

  # Extract chef data from response
  def get_chef(response_body)
    id = response_body["items"][0].dig('fields','chef','sys','id')
    return nil unless id

    chef = response_body['includes']['Entry'].select {|asset| asset['sys']['id'] == id }[0]
    chef['fields']['name']
  end

  def parse_response(response)
    response_body = JSON.parse(response)
    item = response_body["items"][0]
    # puts response_body
    if item.present?
      Recipe.new(
        id: item['sys']['id'],
        title: item['fields']['title'],
        image: get_photo(response_body),
        tags: get_tags(response_body),
        description: item['fields']['description'],
        chef_name: get_chef(response_body)
      )
    end
  end
  
end