module ContentDelivery
  module Base
    CONFIG_ATTR = [:base_url, :space_id, :access_token, :env_id, :recipe_content_id]
    # Define methods to get Content Deliver Configs
    CONFIG_ATTR.each do |attr|
      define_method attr do
        AppConfig.content_delivery.send(attr)
      end
    end
    
    def default_headers
      {
        'Content-Type' => 'application/json'
      }
    end

    def default_params
      {
        access_token: access_token,
      }
    end

    def endpoints
      {
        entries: "#{base_url}/spaces/#{space_id}/environments/#{env_id}/entries/",
      }
    end
    
    # returns [status, response]
    def get_request(url:, headers: default_headers, params: default_params)
      response = HTTParty.get(url,
        headers: headers,
        query: params
      )
      log_response response

      return [response.code == 200, response]
  
    rescue HTTParty::Error, Net::OpenTimeout => e
      handle_exception(e)
      return [false, nil]
    end

    # Default method for handling exception
    def handle_exception(e)
      Rails.logger.error "[ContentDelivery][FAILURE]"
      Rails.logger.error e.message
      Rails.logger.error e.backtrace.join("\n")
    end

    def log_response(response)
      if response.code == 200
        Rails.logger.info "[ContentDelivery][SUCCESS]"
      else
        Rails.logger.error "[ContentDelivery][FAILURE] [CODE #{response.code}]: #{response.body}"
      end
      
    end
  end
end