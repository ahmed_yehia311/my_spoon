class ContentDelivery::ListRecipes
  include ContentDelivery::Base
  
  def request_params
    default_params.merge({
      content_type: recipe_content_id,
      select: "fields.title,fields.photo,sys.id"
    })
  end

  def call
    status, response = get_request(url: endpoints[:entries],
      headers: default_headers,
      params: request_params
    )

    status ? parse_response(response.body) : []
  end

  def parse_response(response)
    response_body = JSON.parse(response)
    recipes = []
    images = {}
    response_body['includes']['Asset'].each do |asset|
      id = asset['sys']['id']
      images[id] = {
        title: asset['fields']['title'],
        url: asset['fields']['file']['url']
      }
    end
    response_body['items'].each do |res|
      image_id = res['fields']['photo']['sys']['id']
      recipes << Recipe.new(id: res['sys']['id'], title: res['fields']['title'], image: images[image_id])
    end
    recipes
  end
  
end