module ApplicationHelper
  def styled_description(text)
    text = decorate(text)
    text = handle_links(text)
    text.html_safe
  end

  def decorate(text)
    text = text.gsub(/\*(.*)\*/, "<b>\\1</b>")
    text = text.gsub(/__([^\_]*)__/, "<i>\\1</i>")
  end

  def handle_links(text)
    text = text.gsub(/\[(.*)\]\((.*)\)/, "<a href='\\2' target='_blank'>\\1</a>")
  end
end
